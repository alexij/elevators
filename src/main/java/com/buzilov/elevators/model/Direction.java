package com.buzilov.elevators.model;

public enum Direction{
    NONE, UP, DOWN
}