package com.buzilov.elevators.model;

import com.buzilov.elevators.exceptions.IllegalFloorException;
import com.buzilov.elevators.utils.ElevatorProperties;

public class Elevator {
    private int number;
    private int currentFloor;
    private int targetFloor;
    private Direction direction;

    public Elevator() {
    }

    public Elevator(int number, int currentFloor, int targetFloor) {
        this.number = number;
        this.currentFloor = currentFloor;
        this.targetFloor = targetFloor;
    }

    public Elevator(int number, int currentFloor, int targetFloor, Direction direction) {
        this.number = number;
        this.currentFloor = currentFloor;
        this.targetFloor = targetFloor;
        this.direction = direction;
    }

    public int getNumber() {
        return number;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getTargetFloor() {
        return targetFloor;
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public String toString() {
        return String.format("Elevator: [number = %s][from: %s][to: %s]", number, currentFloor, targetFloor);
    }

    public static class Builder{
        private int number;
        private int currentFloor = 1;
        private int targetFloor = 1;
        private Direction direction;

        public Builder setNumber(int number){
            this.number = number < 0 ? -number : number;
            return this;
        }

        public Builder setCurrentFloor(int currentFloor) throws IllegalFloorException{
            if (currentFloor <= 0 || currentFloor > ElevatorProperties.getMaxFloor())
                throw new IllegalFloorException("Wrong elevator's current floor! Correct range: from 1 to " + ElevatorProperties.getMaxFloor()
                                               + ", actual: " + currentFloor);

            this.currentFloor = currentFloor;

            return this;
        }

        public Builder setTargetFloor(int targetFloor) throws IllegalFloorException{
            if (targetFloor <= 0 || targetFloor > ElevatorProperties.getMaxFloor())
                throw new IllegalFloorException("Wrong elevator's target floor! Correct range: from 1 to " + ElevatorProperties.getMaxFloor()
                        + ", actual: " + targetFloor);
            this.targetFloor = targetFloor;

            return this;
        }

        public Elevator build(){
            if (currentFloor == targetFloor) this.direction = Direction.NONE;
            else this.direction = currentFloor < targetFloor ? Direction.UP : Direction.DOWN;

            return new Elevator(number, currentFloor, targetFloor, direction);
        }
    }
}
