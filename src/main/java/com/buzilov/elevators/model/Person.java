package com.buzilov.elevators.model;

import com.buzilov.elevators.exceptions.IllegalFloorException;
import com.buzilov.elevators.utils.ElevatorProperties;
import com.buzilov.elevators.exceptions.IllegalDirectionException;

public class Person {
    private int currentFloor;
    private Direction direction;

    public Person() {
    }

    public Person(int currentFloor, Direction direction) {
        this.currentFloor = currentFloor;
        this.direction = direction;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public String toString() {
        return String.format("Person: [current floor = %d], [direction: %s]", currentFloor, direction);

    }

    public static class Builder{
        private int currentFloor = 1;
        private Direction direction;

        public Builder setCurrentFloor(int currentFloor) throws IllegalFloorException{
            if (currentFloor <= 0 || currentFloor > ElevatorProperties.getMaxFloor())
                throw new IllegalFloorException("Wrong person's floor! Correct range: from 1 to " + ElevatorProperties.getMaxFloor()
                        + ", actual: " + currentFloor);
            this.currentFloor = currentFloor;

            return this;
        }

        public Builder setDirection(Direction direction) {
            this.direction = direction;

            return this;
        }

        public Person build() throws IllegalDirectionException{
            if (direction == Direction.NONE){
                throw new IllegalDirectionException("A Person cannot have \"NONE\" direction.");
            }

            if (direction == Direction.UP && this.currentFloor == ElevatorProperties.getMaxFloor())
                throw new IllegalDirectionException("A Person cannot go in \"UP\" direction while being on the highest floor.");
            else if (direction == Direction.DOWN && this.currentFloor == 1)
                throw new IllegalDirectionException("A Person cannot go in \"DOWN\" direction while being on the 1st floor.");

            return new Person(currentFloor, direction);
        }
    }
}
