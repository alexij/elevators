package com.buzilov.elevators.exceptions;

public class IllegalFloorException extends Exception {
    public IllegalFloorException(String message){
        super(message);
    }
}
