package com.buzilov.elevators.exceptions;

public class IllegalDirectionException extends Exception{
    public IllegalDirectionException(String message){
        super(message);
    }

}
