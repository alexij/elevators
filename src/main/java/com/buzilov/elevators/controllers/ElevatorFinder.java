package com.buzilov.elevators.controllers;

import com.buzilov.elevators.model.Direction;
import com.buzilov.elevators.model.Elevator;
import com.buzilov.elevators.model.Person;

import java.util.List;

public class ElevatorFinder {
    static final byte LOW_PRIORITY = 1;
    static final byte MEDIUM_PRIORITY = 2;
    static final byte HIGH_PRIORITY = 3;
    static final byte MAX_PRIORITY = 4;

    // Default constructor just to extend this class if needed.
    public ElevatorFinder() {
    }

    public Elevator getAppropriateElevator(List<Elevator> elevators,
                                           Person person){
        if (elevators.isEmpty()) return null;
        if (elevators.size() == 1) return elevators.get(0);

        Elevator expectedElevator = elevators.get(0);

        byte currentPriority = getPriority(expectedElevator, person);
        int currentDifference = getPotentialFloorsDifference(expectedElevator, person, currentPriority);

        int potentialDifference;
        byte potentialPriority;

        for (int i = 1; i < elevators.size(); i++){
            potentialPriority = getPriority(elevators.get(i), person);
            potentialDifference = getPotentialFloorsDifference(elevators.get(i), person, potentialPriority);

            if (potentialPriority > currentPriority){
                expectedElevator = elevators.get(i);
                currentDifference = potentialDifference;
                currentPriority = potentialPriority;
            }else if (potentialPriority == currentPriority && potentialDifference <= currentDifference){
                expectedElevator = elevators.get(i);
                currentDifference = potentialDifference;
            }

        }

        return expectedElevator;
    }

    public byte getPriority(Elevator elevator, Person person){
        if (elevator.getDirection() == person.getDirection() && isPersonFloorInRangeOfElevatorFloors(elevator, person))
            return MAX_PRIORITY;

        if (elevator.getTargetFloor() == person.getCurrentFloor())
            return HIGH_PRIORITY;

        if (elevator.getCurrentFloor() == person.getCurrentFloor())
            return MEDIUM_PRIORITY;

        return LOW_PRIORITY;
    }

    public boolean isPersonFloorInRangeOfElevatorFloors(Elevator elevator, Person person) {
        return (person.getDirection() == Direction.UP
                && elevator.getTargetFloor() >= person.getCurrentFloor() && elevator.getCurrentFloor() < person.getCurrentFloor())
                || (person.getDirection() == Direction.DOWN
                && elevator.getTargetFloor() <= person.getCurrentFloor() && elevator.getCurrentFloor() > person.getCurrentFloor());
    }

    public int getPotentialFloorsDifference(Elevator elevator, Person person, byte priority){
        int difference;

        if (priority == MAX_PRIORITY){
            difference = Math.abs(elevator.getCurrentFloor() - person.getCurrentFloor());
        }
        else{
            difference = Math.abs(elevator.getTargetFloor() - person.getCurrentFloor())
                    + Math.abs(elevator.getCurrentFloor() - elevator.getTargetFloor());
        }

        return difference;
    }
}
