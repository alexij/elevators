package com.buzilov.elevators.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ElevatorProperties {
    static Properties properties = new Properties();
    static InputStream inputStream = null;

    private ElevatorProperties(){}

    public static int getMaxFloor(){
        try (InputStream inputStream = new FileInputStream("src/main/resources/config.properties")){
            properties.load(inputStream);
            return Integer.valueOf(properties.getProperty("maxFloor"));

        }catch (IOException io){
            io.printStackTrace();
        }

        return 0;
    }
}
