import com.buzilov.elevators.controllers.ElevatorFinder;
import com.buzilov.elevators.exceptions.IllegalDirectionException;
import com.buzilov.elevators.exceptions.IllegalFloorException;
import com.buzilov.elevators.model.Direction;
import com.buzilov.elevators.model.Elevator;
import com.buzilov.elevators.model.Person;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class ElevatorFinderTest {
    List<Elevator> elevatorsList1;
    List<Elevator> elevatorsList2;
    List<Elevator> elevatorsList3;
    List<Elevator> elevatorsList4;
    List<Elevator> elevatorsList5;
    List<Elevator> elevatorsList6;
    List<Elevator> elevatorsList7;

    List<Person> people;

    final static Logger log = Logger.getLogger(ElevatorFinderTest.class.getName());
    private double totalTimeSpent = 0;
    private int testCount = 1;

    @BeforeSuite
    public void initElevators() throws IllegalDirectionException, IllegalFloorException {
        elevatorsList4 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(100).setTargetFloor(99).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(2).setTargetFloor(96).build(),
                Elevator.builder().setNumber(3).setCurrentFloor(95).setTargetFloor(94).build(),
                Elevator.builder().setNumber(4).setCurrentFloor(10).setTargetFloor(1).build()
        ));

        elevatorsList5 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(2).setTargetFloor(2).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(6).setTargetFloor(3).build(),
                Elevator.builder().setNumber(3).setCurrentFloor(4).setTargetFloor(2).build()
        ));

        elevatorsList3 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(100).setTargetFloor(2).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(5).setTargetFloor(4).build(),
                Elevator.builder().setNumber(3).setCurrentFloor(2).setTargetFloor(4).build()
        ));

        elevatorsList2 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(100).setTargetFloor(2).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(5).setTargetFloor(3).build(),
                Elevator.builder().setNumber(3).setCurrentFloor(7).setTargetFloor(4).build()
        ));

        elevatorsList1 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(100).setTargetFloor(2).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(1).setTargetFloor(2).build()
        ));

        elevatorsList6 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(7).setTargetFloor(9).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(3).setTargetFloor(4).build(),
                Elevator.builder().setNumber(3).setCurrentFloor(10).setTargetFloor(5).build()
        ));

        elevatorsList7 = new ArrayList<>(Arrays.asList(
                Elevator.builder().setNumber(1).setCurrentFloor(7).setTargetFloor(9).build(),
                Elevator.builder().setNumber(2).setCurrentFloor(6).setTargetFloor(8).build(),
                Elevator.builder().setNumber(3).setCurrentFloor(2).setTargetFloor(3).build(),
                Elevator.builder().setNumber(4).setCurrentFloor(4).setTargetFloor(3).build()
        ));

        people = new ArrayList<>(Arrays.asList(
                Person.builder().setCurrentFloor(3).setDirection(Direction.UP).build(),
                Person.builder().setCurrentFloor(4).setDirection(Direction.DOWN).build(),
                Person.builder().setCurrentFloor(4).setDirection(Direction.DOWN).build(),
                Person.builder().setCurrentFloor(96).setDirection(Direction.DOWN).build(),
                Person.builder().setCurrentFloor(5).setDirection(Direction.DOWN).build(),
                Person.builder().setCurrentFloor(5).setDirection(Direction.UP).build(),
                Person.builder().setCurrentFloor(5).setDirection(Direction.UP).build()
        ));

    }

    @DataProvider(name = "elevatorsForTest")
    public Object[][] dataProviderForElevatorsTest(){
        return new Object[][]{
                {
                        elevatorsList1,
                        people.get(0),
                        elevatorsList1.get(1)
                },

                {
                        elevatorsList2,
                        people.get(1),
                        elevatorsList2.get(1)
                },

                {
                        elevatorsList3,
                        people.get(2),
                        elevatorsList3.get(1)
                },

                {
                        elevatorsList4,
                        people.get(3),
                        elevatorsList4.get(1)
                },

                {
                        elevatorsList5,
                        people.get(4),
                        elevatorsList5.get(1)
                },

                {
                        elevatorsList6,
                        people.get(5),
                        elevatorsList6.get(2)
                },

                {
                        elevatorsList7,
                        people.get(6),
                        elevatorsList7.get(3)
                }
        };
    }

    @Test(dataProvider = "elevatorsForTest")
    public void testGetAppropriateElevator(List<Elevator> elevators, Person person, Elevator expectedElevator) {
        long timeOnTheStart = System.nanoTime();

        ElevatorFinder elevatorFinder = new ElevatorFinder();
        Elevator actualElevator = elevatorFinder.getAppropriateElevator(elevators, person);
        assertNotEquals(actualElevator, null);
        assertEquals(actualElevator, expectedElevator);

        long timeOnTheEnd = System.nanoTime() - timeOnTheStart;

        StringBuilder elevatorsInfo = new StringBuilder();
        for (Elevator elevator : elevators)
            elevatorsInfo.append(elevator + "\n");

        log.info("\n[Test #" + testCount + " - " + actualElevator + "]\n" +
                "[Elevators]\n" +
                elevatorsInfo + "\n" +
                "[Person]: " + person + "\n" +
                "[Expected elevator]: " + expectedElevator + "\n" +
                "[Actual elevator]: " + actualElevator + "\n" +
                "Time: " + timeOnTheEnd + " nanoseconds.\n\n");

        testCount++;
        totalTimeSpent += timeOnTheEnd;
    }

    @AfterSuite
    public void printExecutionTime(){
        log.info("Total execution time: " + totalTimeSpent);
    }
}